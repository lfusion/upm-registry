# Upm Registry
This repository is used as a Registry to host LFusion Upm packages.
## Installation
 - Go to `Edit/Project Settings/Package Manager`
 - Add a new Scoped Registry with:
	 - `Name : LFusion Gitlab`
	 - `URL : https://gitlab.com/api/v4/projects/54412099/packages/npm`
	 - `Scope(s) : com.lfusion`
 - Apply
